import { Module } from '@nestjs/common';
import { TopupService } from './topup.service';
import { TopupController } from './topup .controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Topup } from './topup.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Topup])],
  providers: [TopupService],
  controllers: [TopupController]
})
export class TopupModule {}
