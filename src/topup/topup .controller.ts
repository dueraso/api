import { Controller } from '@nestjs/common';
import {
  Body,
  Get,
  Param,
  Post,
} from '@nestjs/common';
import { Topup } from './topup.entity';
import { TopupService } from './topup.service';

@Controller('topup')
export class TopupController {
    constructor(private topupService: TopupService) {}

  @Get()
  findAll() {
    return this.topupService.getTopup();
  }

  @Get(':id')
  findOne(@Param('id') id) {
    return this.topupService.findOne(id);
  }

  @Post('/create') create(@Body() topup: Topup) {
    return this.topupService.create(topup);
  }
}
