import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Topup } from './topup.entity';

@Injectable()
export class TopupService {
    constructor(
    @InjectRepository(Topup) private topupRepository: Repository<Topup>,
    ) { }
    
  async getTopup(): Promise<Topup[]> {
    return await this.topupRepository.find();
  }

  findOne(id: string): Promise<Topup> {
    return this.topupRepository.findOne(id);
  }

  async create(topup: Topup) {
    this.topupRepository.save(topup);
  }
}
