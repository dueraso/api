import { Controller } from '@nestjs/common';
import {
  Body,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { Profile } from './profile.entity';
import { ProfileService } from './profile.service';

@Controller('profile')
export class ProfileController {
    constructor(private profileService: ProfileService) {}

  @Get()
  findAll() {
    return this.profileService.getProfile();
  }

  @Get(':id')
  findOne(@Param('id') id) {
    return this.profileService.findOne(id);
  }

  @Post('/create') create(@Body() profile: Profile) {
    return this.profileService.create(profile);
  }
}
