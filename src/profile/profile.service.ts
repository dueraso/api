import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Profile } from './profile.entity';

@Injectable()
export class ProfileService {
    constructor(
    @InjectRepository(Profile) private profileRepository: Repository<Profile>,
    ) { }
    
  async getProfile(): Promise<Profile[]> {
    return await this.profileRepository.find();
  }

  findOne(id: string): Promise<Profile> {
    return this.profileRepository.findOne(id);
  }

  async create(profile: Profile) {
    this.profileRepository.save(profile);
  }
}
