import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Transfer } from './transfer.entity';

@Injectable()
export class TransferService {
    constructor(
    @InjectRepository(Transfer) private topupRepository: Repository<Transfer>,
    ) { }
    
  async getTransfer(): Promise<Transfer[]> {
    return await this.topupRepository.find();
  }

  findOne(id: string): Promise<Transfer> {
    return this.topupRepository.findOne(id);
  }

  async create(transfer: Transfer) {
    this.topupRepository.save(transfer);
  }
}
