import { Controller } from '@nestjs/common';
import {
  Body,
  Get,
  Param,
  Post,
} from '@nestjs/common';
import { Transfer } from './transfer.entity';
import { TransferService } from './transfer.service';

@Controller('transfer')
export class TransferController {
    constructor(private topupService: TransferService) {}

  @Get()
  findAll() {
    return this.topupService.getTransfer();
  }

  @Get(':id')
  findOne(@Param('id') id) {
    return this.topupService.findOne(id);
  }

  @Post('/create') create(@Body() transfer: Transfer) {
    return this.topupService.create(transfer);
  }
}
