import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class Transfer extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255 })
    code_ref: string;

    @Column('int')
    my_profile_id: number;
    
    @Column('int')
    to_profile_id: number;

    @Column({ length: 13 })
    total: string;
    
    @CreateDateColumn()
    createDate: Date;

    @UpdateDateColumn()
    updateDate: Date;
}